from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse

from .models import Region, Candidato
# Create your views here.

def index(request):
    latest_question_list = Region.objects.order_by('-region_txt')[:5]
    context = {'latest_question_list': latest_question_list}
    return render(request, 'votos/index.html', context)

def detalle(request, region_id):
    region = get_object_or_404(Region, pk=region_id)
    return render(request, 'votos/detalle.html', {'region': region})

def votar(request, region_id):
    region = get_object_or_404(Region, pk=region_id)
    try:
        selected_opcion = region.candidato_set.get(pk=request.POST['candidato'])
    except (KeyError, Candidato.DoesNotExist):
        return render(request, 'votos/detalle.html',{
            'region' : region,
            'error_message': "No has seleccionado un candidato.",
        })
    else:
        selected_opcion.votos += 1
        selected_opcion.save()
        return HttpResponseRedirect(reverse('votos:resultados', args=(region.id,)))

def resultados(request, region_id):
    region = get_object_or_404(Region, pk=region_id)
    return render(request, 'votos/resultados.html', {'region': region})